<?php

namespace App\Controller;

use App\Entity\Listes;
use App\Form\ListesType;
use App\Repository\ListesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TodolistController extends AbstractController
{
  //Page accueil---------------------------------------------------------------------------------------------------------------------------
  /**
   * @Route("/", name="home")
   */
  public function home( ListesRepository $listeRepo)
  {
      $allListe = $listeRepo->findBy(array(), array('dateCreation' => 'DESC'));
      return $this->render('todolist/index.html.twig', ['controller_name' => 'TodolistController', 'title' => "Home", 'allListe' => $allListe]);
    }
    
    //Page liste en dur---------------------------------------------------------------------------------------------------------------------------
    /**
     * @Route("/listeEnDur", name="listeEnDur")
     */
    public function listeEnDur( ListesRepository $listeRepo)
    {
        return $this->render('todolist/liste1.html.twig');
    }

    //page de formulaire avec deux chemins : créaction d'une liste et modification d'une liste---------------------------------------------------
    /**
     * @Route("/liste/form", name="liste_forms")
     * @Route("/liste/{id}/edit", name="liste_edit")
     */
    public function formulaire(Listes $liste = null, Request $request, EntityManagerInterface $manager)
    {   
        //condition de vérification de l'existence de la liste pour savoir si on doit créer une nouvelle liste ou non
        if (!$liste) {
            $liste = new Listes();
        }

        //appel de la créaction du formulaire
        $form = $this->createForm(ListesType::class, $liste);
        $form->handleRequest($request);

        //condition de vérification de l'existence de la liste pour savoir si on doit l'update 
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$liste->getId()) {
                $liste->setValider(false);
                $liste->setDateCreation(new \DateTimeImmutable());
            }
            $manager->persist($liste);
            $manager->flush();
            return $this->redirectToRoute('home');
        }
        //ou si on le créer

        return $this->render('todolist/form.html.twig', ['formListe' => $form->createView(), 'editMode' => $liste->getId() !== null]);
    }

    /**
      * 
      * @Route("/{id}", name="supprimer_liste")
      */
     public function supprimerFichier(Request $request, string $id, ListesRepository $repo, EntityManagerInterface $manager)
     {
         $liste = $repo->findOneBy(['id' => $id]);
         $manager->remove($liste);
         $manager->flush();
        
         return $this->redirect($request->headers->get('referer'));
     }
}
